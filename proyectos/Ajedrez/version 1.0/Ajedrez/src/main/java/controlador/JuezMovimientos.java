/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controlador;

import modelo.Tablero;

/**
 *
 * @author Ronald
 */
public class JuezMovimientos {

    Tablero tablero;

    public JuezMovimientos(Tablero tablero) {
        this.tablero = tablero;
    }
    
    public boolean isMovimientoDiagonal(int filaActual, int columnaActual, int filaDestino, int columnaDestino){
        int diferenciaFila = filaActual - filaDestino;
        int diferenciaColumna = columnaActual - columnaDestino;
        return Math.abs(diferenciaFila) == Math.abs(diferenciaColumna); //Verifica que sea un movimiento en diagonal
    }
    
    public boolean isMovimientoFila(int filaActual, int filaDestino){
        return filaActual == filaDestino; //Verifica que sea un movimiento en diagonal
    }
    
        public boolean isMovimientoColumna(int columnaActual, int columnaDestino){
        return columnaActual == columnaDestino; //Verifica que sea un movimiento en diagonal
    }

    public boolean validarDiagonalSuperiorDerecha(int filaActual, int columnaActual, int filaDestino, int columnaDestino) {
       int fila= filaActual +1;
        for (int columna = (columnaActual + 1); columna < columnaDestino; columna++) {
            if (!tablero.isPosicionVacia(fila, columna)) {
                return false;
            }
            System.out.println("fila:" +fila+ "columna:" +columna);
            fila++;
        }//Fin del for
        return true;
    }
    
    public boolean validarDiagonalSuperiorIzquierda(int filaActual, int columnaActual, int filaDestino, int columnaDestino) {
       int fila= filaActual +1;
        for (int columna = (columnaActual - 1); columna < columnaDestino; columna--) {
            if (!tablero.isPosicionVacia(fila, columna)) {
                return false;
            }
            fila++;
        }//Fin del for
        return true;
    }
    
    public boolean validarDiagonalInferiorDerecha(int filaActual, int columnaActual, int filaDestino, int columnaDestino) {
       int fila= filaActual -1;
        for (int columna = (columnaActual + 1); columna < columnaDestino; columna++) {
            if (!tablero.isPosicionVacia(fila, columna)) {
                return false;
            }
            fila--;
        }//Fin del for
        return true;
    }
    
    public boolean validarDiagonalInferiorIzquierda(int filaActual, int columnaActual, int filaDestino, int columnaDestino) {
       int fila= filaActual -1;
        for (int columna = (columnaActual - 1); columna < columnaDestino; columna--) {
            if (!tablero.isPosicionVacia(fila, columna)) {
                return false;
            }
            fila--;
        }//Fin del for
        return true;
    }
    
    public boolean validarFilaAscendente(int filaActual, int columnaActual, int filaDestino){
        for (int fila = (filaActual - 1); fila < filaDestino; fila--) {
            if (!tablero.isPosicionVacia(fila, columnaActual)) {
                return false;
            }
        }//Fin del for
        return true;
    }
    
    public boolean validarFilaDescendente(int filaActual, int columnaActual, int filaDestino){
        for (int fila = (filaActual + 1); fila < filaDestino; fila++) {
            if (!tablero.isPosicionVacia(fila, columnaActual)) {
                return false;
            }
        }//Fin del for
        return true;
    }
    
    public boolean validarColumnaDerecha(int filaActual, int columnaActual, int columnaDestino){
        for (int columna = (columnaActual + 1); columna < columnaDestino; columna++) {
            if (!tablero.isPosicionVacia(filaActual, columnaActual)) {
                return false;
            }
        }//Fin del for
        return true;
    }
    
     public boolean validarColumnaIzquierda(int filaActual, int columnaActual, int columnaDestino){
        for (int columna = (columnaActual - 1); columna < columnaDestino; columna--) {
            if (!tablero.isPosicionVacia(filaActual, columnaActual)) {
                return false;
            }
        }//Fin del for
        return true;
    }

}
