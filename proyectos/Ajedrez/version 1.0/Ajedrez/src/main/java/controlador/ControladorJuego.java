/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controlador;

import javax.swing.ImageIcon;
import modelo.Alfil;
import modelo.Caballo;
import modelo.Estadisticas;
import modelo.Jugador;
import modelo.Peon;
import modelo.Pieza;
import modelo.Reina;
import modelo.Rey;
import modelo.Tablero;
import modelo.Torre;

/**
 *
 * @author Ronald
 */
public class ControladorJuego {
    
    private Estadisticas estadisticas;
    private Jugador jugadores;
    private Tablero tablero;
    private JuezMovimientos juez;
    private final byte color_1= 1;
    private final byte color_2= 2;
     private final byte direccion_1= 1;
    private final byte direccion_2= 2;

    public ControladorJuego() {
        estadisticas = new Estadisticas();
        
        
        estadisticas.setJugador(new Jugador(1,"Luis"));
        estadisticas.setJugador(new Jugador(2,"Mathias"));
        
        tablero = new Tablero();
        inicializarTablero();
        
        juez = new JuezMovimientos(tablero);
        System.out.println(tablero);
        
        tablero.getPieza(1,3).mover(1, 3, 2, 3, tablero);
        tablero.getPieza(0,2).mover(0,2, 4, 7, tablero);
         System.out.println(tablero);
         
         System.out.println(juez.validarDiagonalSuperiorDerecha(2, 4, 4, 7));
        
    }
    
    public void inicializarTablero(){
        Pieza torreColor1_1 = new Torre(color_1);
        Pieza torreColor1_2 = new Torre(color_1);
        Pieza torreColor2_1 = new Torre(color_2);
        Pieza torreColor2_2 = new Torre(color_2);

        tablero.setPieza(0, 0, torreColor1_1);
        tablero.setPieza(0, 7, torreColor1_2);
        tablero.setPieza(7, 0, torreColor2_1);
        tablero.setPieza(7, 7, torreColor2_2);

        Pieza caballoColor1_1 = new Caballo(color_1);
        Pieza caballoColor1_2 = new Caballo(color_1);
        Pieza caballoColor2_1 = new Caballo(color_2);
        Pieza caballoColor2_2 = new Caballo(color_2);

        tablero.setPieza(0, 1, caballoColor1_1);
        tablero.setPieza(0, 6, caballoColor1_2);
        tablero.setPieza(7, 1, caballoColor2_1);
        tablero.setPieza(7, 6, caballoColor2_2);
        
        Pieza alfilColor1_1 = new Alfil(color_1);
        Pieza alfilColor1_2 = new Alfil(color_1);
        Pieza alfilColor2_1 = new Alfil(color_2);
        Pieza alfilColor2_2 = new Alfil(color_2);

        tablero.setPieza(0, 2, alfilColor1_1);
        tablero.setPieza(0, 5, alfilColor1_2);
        tablero.setPieza(7, 2, alfilColor2_1);
        tablero.setPieza(7, 5, alfilColor2_2);
        
        Pieza reinaColor1 = new Reina(color_1);
        Pieza reinaColor2 = new Reina(color_2);

        tablero.setPieza(0, 4, reinaColor1);
        tablero.setPieza(7, 4, reinaColor1);
        //tablero.setPieza(2, 0, reinaBlanca2);
        
        Pieza reyColor1 = new Rey(color_1);
        Pieza reyColor2 = new Rey(color_2);   
        
        tablero.setPieza(0, 3, reyColor1);
        tablero.setPieza(7, 3, reyColor2);
        
        //int x=79;
        for(int columna=0; columna < 8; columna++){
            Pieza peon;
            peon = new Peon(color_1, direccion_1);
            tablero.setPieza(1, columna,peon);
            //x+=89;
        }
        
        //x=79;
        for(int columna=0; columna < 8; columna++){
            Pieza peon = new Peon(color_2, direccion_2);
            tablero.setPieza(6, columna, peon);
        }
    }
    
    
}
