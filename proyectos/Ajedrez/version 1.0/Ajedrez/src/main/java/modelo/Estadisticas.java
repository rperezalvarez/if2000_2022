/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package modelo;

/**
 *
 * @author Ronald
 */
public class Estadisticas {
    private Pieza piezasComidas[];
    private Jugador jugadores[];

    public Estadisticas() {
        piezasComidas = new Pieza[30];
        jugadores = new Jugador[2];
    }
    
    public void setPiezaComida(Pieza pieza,int posicion){
        if (piezasComidas[posicion]== null)
            piezasComidas[posicion] = pieza;
    }
    
    public void setPiezaComida(Pieza pieza){
        int posicion = getPosicionVaciaPiezasComidas();
        if(posicion != -1){
            piezasComidas[posicion] = pieza;
        }
    }
    
    private int getPosicionVaciaPiezasComidas(){
        for(int posicion=0; posicion < piezasComidas.length; posicion++){
            if( piezasComidas[posicion]== null)
                return posicion;
        }
        return -1;
    }
    
    public void setJugador(Jugador jugador, int posicion){
        if (jugadores[posicion]== null)
            jugadores[posicion] = jugador;
    }
    
    public void setJugador(Jugador jugador){
        int posicion = getPosicionVaciaJugador();     
        if (posicion != -1)
            jugadores[posicion] = jugador;
    }
    
    private int getPosicionVaciaJugador(){
        for(int posicion=0; posicion < jugadores.length; posicion++){
            if( jugadores[posicion]== null)
                return posicion;
        }
        return -1;
    }
    
    public Pieza[] getPiezasComidasColor(byte color){
        Pieza piezasComidasColor[] = new Pieza[15];
        int indice=0;
        for(int posicion=0; posicion < piezasComidas.length; posicion++){
            if( piezasComidas[posicion].getColor()== color){
                piezasComidasColor[indice]= piezasComidas[posicion];
            }
        }
        return piezasComidasColor;
    }
    public Pieza[] getPiezasComidasColor2(byte color){
        Pieza piezasComidasColor[] = new Pieza[15];
        int indice=0;
        for (Pieza piezasComida : piezasComidas) {
            if (piezasComida.getColor() == color) {
                piezasComidasColor[indice] = piezasComida;
            }
        }
        return piezasComidasColor;
    }
}
