/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author UCR
 */
public class Peon extends Pieza {

    private byte direccion;
    private boolean primerMovimiento;

    public Peon(byte color, byte direccion) {
        super("Peon", color);
        this.direccion = direccion;
        primerMovimiento = true;
    }

    @Override
    public boolean mover(int filaActual, int columnaActual, int filaDestino, int columnaDestino, Tablero tablero) {
        int diferenciaFila;
        int diferenciaColumna;
        boolean seMueve = false;
        diferenciaColumna = columnaActual - columnaDestino;
        if (direccion == 1) {
            diferenciaFila = filaDestino - filaActual;
        } else {
            diferenciaFila = filaActual - filaDestino;
        }
        if (tablero.getPieza(filaDestino, columnaDestino) != null) {
            if (tablero.getPieza(filaDestino, columnaDestino).getColor() != getColor() && ((diferenciaFila == 1) && ((diferenciaColumna == 1) || (diferenciaColumna == -1)))) {
                seMueve = true;
                tablero.setPiezaComida(tablero.getPieza(filaDestino, columnaDestino));
            }
        } else {
            if ((diferenciaColumna == 0) && ((diferenciaFila == 1) || ((diferenciaFila == 2) && (primerMovimiento)))) {
                seMueve = true;
            }
        }
        if (seMueve) {
            if (primerMovimiento) {
                primerMovimiento = false;
            }
            tablero.setPieza(filaDestino, columnaDestino, tablero.getPieza(filaActual, columnaActual));
            tablero.setPieza(filaActual, columnaActual, null);
            return true;
        }
        return false;
    }
}
