/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author UCR
 */
public class Rey extends Pieza {

    public Rey(byte color) {
        super("Rey", color);
    }

    @Override
    public boolean mover(int filaActual, int columnaActual, int filaDestino, int columnaDestino, Tablero tablero) {
        System.out.println("Me muevo como Rey");
        return true;
    }

}
