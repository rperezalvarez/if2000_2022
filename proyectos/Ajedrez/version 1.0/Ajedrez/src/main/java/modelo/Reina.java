/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author UCR
 */
public class Reina extends Pieza {

    public Reina(byte color) {
        super("Reina", color);
    }

    @Override
    public boolean mover(int filaActual, int columnaActual, int filaDestino, int columnaDestino, Tablero tablero) {
        int diferenciaFila = filaActual - filaDestino;
        int diferenciaColumna = columnaActual - columnaDestino;

        if (Math.abs(diferenciaFila) == Math.abs(diferenciaColumna)) { //Verifica que sea un movimiento en diagonal
            if (diferenciaFila < 0 && diferenciaColumna < 0) {//Verifica si es la diagonal superior derecha
                int fila = filaActual + 1;
                for (int columna = (columnaActual + 1); columna < columnaDestino; columna++) {//Verifica si es la diagonale esta libre
                    if (!tablero.isPosicionVacia(fila, columna)) {
                        return false;
                    }
                    fila++;
                }//Fin del for
            } else {
                if (diferenciaFila < 0 && diferenciaColumna > 0) {//Verifica si es la diagonal superior Izquierda
                    int fila = filaActual + 1;
                    for (int columna = (columnaActual - 1); columna < columnaDestino; columna--) {//Verifica si es la diagonale esta libre
                        if (!tablero.isPosicionVacia(fila, columna)) {
                            return false;
                        }
                        fila++;
                    }//Fin del for
                } else {
                    if (diferenciaFila > 0 && diferenciaColumna < 0) {//Verifica si es la diagonal inferior derecha
                        int fila = filaActual - 1;
                        for (int columna = (columnaActual + 1); columna < columnaDestino; columna++) {//Verifica si es la diagonale esta libre
                            if (!tablero.isPosicionVacia(fila, columna)) {
                                return false;
                            }
                            fila--;
                        }//Fin del for
                    } else {
                        if (diferenciaFila > 0 && diferenciaColumna > 0) {//Verifica si es la diagonal inferior Izquierda
                            int fila = filaActual - 1;
                            for (int columna = (columnaActual - 1); columna < columnaDestino; columna--) {//Verifica si es la diagonale esta libre
                                if (!tablero.isPosicionVacia(fila, columna)) {
                                    return false;
                                }
                                fila--;
                            }//Fin del for
                        }
                    }
                }
            }
        }else{
            if ((filaActual == filaDestino) && (columnaActual < columnaDestino)) {//Valida si el movimiento es sobre la misma fila hacia la derecha
            for (int columna = (columnaActual + 1); columna < columnaDestino; columna++) {// Valida si el recorrido de la fila esta libre
                if (tablero.getPieza(filaActual, columna) != null) {
                    return false;
                }
            }//Fin del for
        } else {
            if ((filaActual == filaDestino) && (columnaActual > columnaDestino)) {
                for (int columna = (columnaActual - 1); columna > columnaDestino; columna--) {
                    if (tablero.getPieza(filaActual, (columna - 1)) != null) {
                        return false;
                    }
                }//Fin del for
            } else {
                if ((columnaActual == columnaDestino) && (filaActual < filaDestino)) {
                    for (int fila = (filaActual + 1); fila < filaDestino; fila++) {
                        if (tablero.getPieza(fila, columnaActual) != null) {
                            return false;
                        }
                    }//Fin del for
                } else {
                    if ((columnaActual == columnaDestino) && (filaActual > filaDestino)) {
                        for (int fila = (filaActual - 1); fila > filaDestino; fila--) {
                            if (tablero.getPieza(fila, columnaActual) != null) {
                                return false;
                            }
                        }//Fin del for
                    }
                }
            }
        }
        }
        return true;
    }
}
