/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author UCR
 */
public abstract class Pieza {
    private String tipo;
    private byte color;

    public Pieza(String tipo, byte color) {
        this.tipo = tipo;
        this.color = color;
    }

    public void setNombre(String tipo) {
        this.tipo = tipo;
    }

    public void setColor(byte color) {
        this.color = color;
    }

    public String getTipo() {
        return tipo;
    }

    public int getColor() {
        return color;
    }

    @Override
    public String toString() {
        return "Pieza{" + "Tipo=" + tipo + ", color=" + color + '}';
    }
    
    public abstract boolean mover(int filaActual,int columnaActual,int filaDestino, int columnaDestino, Tablero tablero);
    
    
}
