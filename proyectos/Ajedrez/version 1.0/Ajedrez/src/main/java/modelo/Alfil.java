/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author UCR
 */
public class Alfil extends Pieza {

    public Alfil(byte color) {
        super("Alfil", color);
    }

    @Override
    public boolean mover(int filaActual, int columnaActual, int filaDestino, int columnaDestino, Tablero tablero) {

        int diferenciaFila = filaActual - filaDestino;
        int diferenciaColumna = columnaActual - columnaDestino;

        if (Math.abs(diferenciaFila) == Math.abs(diferenciaColumna)) { //Verifica que sea un movimiento en diagonal
            if (diferenciaFila < 0 && diferenciaColumna < 0) {//Verifica si es la diagonal superior derecha
                int fila = filaActual + 1;
                for (int columna = (columnaActual + 1); columna < columnaDestino; columna++) {//Verifica si es la diagonale esta libre
                    if (!tablero.isPosicionVacia(fila, columna)) {
                        return false;
                    }
                    fila++;
                }//Fin del for
            } else {
                if (diferenciaFila < 0 && diferenciaColumna > 0) {//Verifica si es la diagonal superior Izquierda
                    int fila = filaActual + 1;
                    for (int columna = (columnaActual - 1); columna < columnaDestino; columna--) {//Verifica si es la diagonale esta libre
                        if (!tablero.isPosicionVacia(fila, columna)) {
                            return false;
                        }
                        fila++;
                    }//Fin del for
                } else {
                    if (diferenciaFila > 0 && diferenciaColumna < 0) {//Verifica si es la diagonal inferior derecha
                        int fila = filaActual - 1;
                        for (int columna = (columnaActual + 1); columna < columnaDestino; columna++) {//Verifica si es la diagonale esta libre
                            if (!tablero.isPosicionVacia(fila, columna)) {
                                return false;
                            }
                            fila--;
                        }//Fin del for
                    } else {
                        if (diferenciaFila > 0 && diferenciaColumna > 0) {//Verifica si es la diagonal inferior Izquierda
                            int fila = filaActual - 1;
                            for (int columna = (columnaActual - 1); columna < columnaDestino; columna--) {//Verifica si es la diagonale esta libre
                                if (!tablero.isPosicionVacia(fila, columna)) {
                                    return false;
                                }
                                fila--;
                            }//Fin del for
                        }
                    }
                }
            }
            if (tablero.getPieza(filaDestino, columnaDestino) != null) {
                if (tablero.getPieza(filaDestino, columnaDestino).getColor() != getColor()) {
                    tablero.setPiezaComida(tablero.getPieza(filaDestino, columnaDestino));
                    tablero.setPieza(filaDestino, columnaDestino, tablero.getPieza(filaActual, columnaActual));
                    tablero.setPieza(filaActual, columnaActual, null);
                    return true;
                } else {
                    return false;
                }
            } else {
                tablero.setPieza(filaDestino, columnaDestino, tablero.getPieza(filaActual, columnaActual));
                tablero.setPieza(filaActual, columnaActual, null);
                return true;
            }
        } else {
            return false;
        }
    }

}
