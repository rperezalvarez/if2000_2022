/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author UCR
 */
public class Caballo extends Pieza{

    public Caballo( byte color) {
        super("Caballo", color);
    }

    @Override
     public boolean mover(int filaActual,int columnaActual,int filaDestino, int columnaDestino, Tablero tablero){
      int diferenciaFila=filaActual-filaDestino;
      int diferenciaColumna=columnaActual-columnaDestino;
      if (isMovimientoValido(diferenciaFila,diferenciaColumna)){
        if(tablero.getPieza(filaDestino,columnaDestino)!=null){
          if(tablero.getPieza(filaDestino,columnaDestino).getColor()!=getColor()){
          tablero.setPiezaComida(tablero.getPieza(filaDestino,columnaDestino));
          }else{return false;}
        }
          tablero.setPieza(filaDestino,columnaDestino,tablero.getPieza(filaActual,columnaActual));
          tablero.setPieza(filaActual,columnaActual,null);
          return true;                          
        }
      
      
      System.out.println("Me muevo como caballo");
    return false;
    }
    
    public boolean isMovimientoValido(int diferenciaFila,int diferenciaColumna){
      if((diferenciaFila==2)&&((diferenciaColumna==1)||(diferenciaColumna==-1))){
        return true;
      }else
      {
        if((diferenciaFila==1)&&((diferenciaColumna==2)||(diferenciaColumna==-2))){
          return true;
        }
        else{
          if((diferenciaFila==-2)&&((diferenciaColumna==1)||(diferenciaColumna==-1))){
            return true;
        }
          else{
            if((diferenciaFila==-1)&&((diferenciaColumna==2)||(diferenciaColumna==-2))){
              return true;
            }
          }
        }
      }
      return false;
    }

}
