/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author UCR
 */
public class Tablero {
    private Pieza tablero[][];
    private Pieza piezaComida;

    public Tablero() {
        tablero=new Pieza[8][8];
    }
    
    public void setPieza(int fila, int columna, Pieza pieza){
        tablero[fila][columna] = pieza;
    }
    
    public boolean isPosicionVacia(int fila, int columna){
        if(tablero[fila][columna] == null){
            return true;
        }
        else{
            return false;
        }
    }
    
    public Pieza getPieza(int fila, int columna){
        return tablero[fila][columna];
    }
    
    public boolean isPosicionValida(int fila, int columna){
        if(fila < 8 && fila >= 0){
            if(columna < 8 && columna >= 0){
                return true;
            }
        }
        return false;
    }
    

    @Override
    public String toString() {
       String mostrar="";
       for (int fila=0;fila<tablero.length;fila++){
           for (int columna=0;columna<tablero[0].length;columna++){
               if (tablero[fila][columna] != null){
                 mostrar+="["+tablero[fila][columna].getTipo()+"]";
               }else{
                   mostrar += "[      ]";
               }
           }
           mostrar+="\n";
       }
       return mostrar;
    }
    
    public int length(){
        return tablero.length;
    }
    
    public int length(int fila){
        return tablero[fila].length;
    }

    public void setPiezaComida(Pieza pieza){
    piezaComida=pieza;
    }
    
    
}
