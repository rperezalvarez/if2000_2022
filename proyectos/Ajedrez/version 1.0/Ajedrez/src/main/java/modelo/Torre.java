/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author UCR
 */
public class Torre extends Pieza {

    public Torre(byte color) {
        super("Torre", color);
    }

    @Override
    public boolean mover(int filaActual, int columnaActual, int filaDestino, int columnaDestino, Tablero tablero) {
        boolean seMueve = false;

        if ((filaActual == filaDestino) && (columnaActual < columnaDestino)) {//Valida si el movimiento es sobre la misma fila hacia la derecha
            for (int columna = (columnaActual + 1); columna < columnaDestino; columna++) {// Valida si el recorrido de la fila esta libre
                if (tablero.getPieza(filaActual, columna) != null) {
                    return false;
                }
            }//Fin del for
            seMueve = true;
        } else {
            if ((filaActual == filaDestino) && (columnaActual > columnaDestino)) {
                for (int columna = (columnaActual - 1); columna > columnaDestino; columna--) {
                    if (tablero.getPieza(filaActual, (columna - 1)) != null) {
                        return false;
                    }
                }//Fin del for
                seMueve = true;
            } else {
                if ((columnaActual == columnaDestino) && (filaActual < filaDestino)) {
                    for (int fila = (filaActual + 1); fila < filaDestino; fila++) {
                        if (tablero.getPieza(fila, columnaActual) != null) {
                            return false;
                        }
                    }//Fin del for
                    seMueve = true;
                } else {
                    if ((columnaActual == columnaDestino) && (filaActual > filaDestino)) {
                        for (int fila = (filaActual - 1); fila > filaDestino; fila--) {
                            if (tablero.getPieza(fila, columnaActual) != null) {
                                return false;
                            }
                        }//Fin del for
                        seMueve = true;
                    }
                }
            }
        }
        if (tablero.getPieza(filaDestino, columnaDestino) != null) {
            if (tablero.getPieza(filaDestino, columnaDestino).getColor() != getColor()) {
                tablero.setPiezaComida(tablero.getPieza(filaDestino, columnaDestino));
                tablero.setPieza(filaDestino, columnaDestino, tablero.getPieza(filaActual, columnaActual));
                tablero.setPieza(filaActual, columnaActual, null);
                return true;
            } else {
                return false;
            }
        } else {
            tablero.setPieza(filaDestino, columnaDestino, tablero.getPieza(filaActual, columnaActual));
            tablero.setPieza(filaActual, columnaActual, null);
            return true;
        }

    }//Fin del método

}
